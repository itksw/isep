# isep - ITk Online Software Enhancement Proposals

## What's this

This is a loose collection of ideas on standards an best practices in ATLAS ITk Online Software.

For the time being there is no formalism in accepting ISEPs. This should be developed over time.
