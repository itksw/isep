
from pydantic import BaseModel, ConfigDict
# from ctypes import c_uint64

REGMAP_REG_READ = 1

class SubregisterModel(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    desc: str
    address: int #c_uint64
    mask: int #c_uint64
    bf_lo: int
    bf_hi: int
    type: int # REGMAP_REG_READ
    endpoints: int # REGMAP_ENDPOINT_0|REGMAP_ENDPOINT_1

class RegisterModel(BaseModel):
    name: str
    subregisters: SubregisterModel

class RegistersModel(BaseModel):
    registers: RegisterModel

if __name__ == '__main__':
    print(RegistersModel.model_json_schema())