# ITk SW Package Proposal

[Original talk in Indico](https://indico.cern.ch/event/1353986/contributions/5865581/attachments/2822997/4930428/240319_ITk_Packages_Proposal.pdf)

Authors:

- Paolo Morettini^1, Carlos Solans^2, Ismet Siral^2, Gerhard Brandt^3, Matthias Wittgen^4, Egor Antipov^5, Vakhtang Tsiskaridze^6

1. INFN-Genova
2. CERN
3. University of Wuppertal
4. SLAC
5. Stony Brook University
6. University of Toronto

## Introduction

In this talk we will try to summarize a discussion on a possible organization of the work
for the Stage 2 of ITk online software.
It is the logical continuation of similar presentations in previous DAQ meetings [link1, link2].
The original proposal is progressively and more or less spontaneously evolving with contributions from several experts and developers. Being a completely open discussion,
we hope that even more people will join in the future.
And, as any open discussion, it is sometimes a bit chaotic and requires
some work to become a complete proposal for the organization of this
important activity, covering all technical and organizational aspects.
Nonetheless, we believe it is important to keep the community informed
and to ask everyone to contribute and help to converge.
The clock is ticking, and it's time to start working.

## Basic Principles

The basic principles are mainly contained in TF recommendations reflected in MG Mandate:

- Developer-friendly environment: institutional participation should be encouraged.
- Community involvement: individual developers or developer teams also should be encouraged.
- Evolution of components of existing codebase must be seriously considered. This is even more important today than an year ago as we have much more already functional high performance code.
- To achieve these results, it was suggested to divide software into independent packages based on agreed and formally defined internal and external interfaces. Each package should be open for institutional and individual contributors and coordinated by dedicated coordinators.

We should add to the above main principles some additional requirements:

- The software should be stable, reliable, based on perspective high performance technologies and provide unified approach for both pixel and strips.

## ITk Readout Distribution

ITk Readout should consists of independent packages:

- Each package is expected to be one git repository.
- Each package should be coordinated by dedicated coordinators.
- Interfaces (API) for packages have to be well defined.
- A language agnostic definition of the API is preferable.
- If API is not needed, only a data format can be defined.
- Package might provide several APIs (C++, Python, …).
- Different  implementations of the same interface can be developed for a work-package.

We should introduce notion of certification:

- Certification should be performed on library/binary level by independent testing tools.
- Any package which pass certification should be allowed for the official use.
- Package without certification should not be used for an official use.
- Two certified packages with the same functionality have to be interchangeable (this will allow continuous integration).
- Certification can be provided on different levels, which reflects the development status e.g.: planning, development, experimental, testing, production.

## Package Repos

-Repos should be in CERN GitLab and public.
-Each repo should follow a standard structure and have standard files for information and bookkeeping (README, CHANGELOG, AUTHORS, ….), which should adhere to standard formats.
-Should have Continuous Integration (CI).
-Should have automated builds of artifacts, containers, packages, …

## API Descriptions

- API description of different packages should follow uniform conventions.
- Due to long future an evolution of the possible interfaces formats should be allowed.
- A standard description tool should be defined for each API, (e.g. Doxygen for C++, …)
- An API Reference should be provided in a human readable way, e.g. rendered in Mkdocs or Sphinx on a *.docs.cern.ch website.

## User Interfaces
- Various Graphics User Interfaces (GUI) and CLI should be proposed to interact with the APIs in a standalone (per package) and connected way.
- Preference of web based GUIs. CLI should adhere to established CLI standards (e.g. docopt).

## General Comments

- The structure proposed on the following slides is not set in stone, it should be refactorable. It should remain possible to break up in the direction of smaller more manageable packages
if a particular package grows too much.

The [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy)

The Unix philosophy emphasizes building simple, compact, clear, modular, and extensible code that can be easily maintained and repurposed by developers other than its creators. The Unix philosophy favors composability as opposed to monolithic design.

- This allows individual groups to take responsibility of certain blocks allowing distribution of work. 
- We propose to start from as small functional units as possible for packages (principle of “do one thing and one thing well”).
- Where the structure of the package is not imposed by external constraints or trivial, but started from scratch one should consider using Domain-Driven Design (DDD) and SOLID principles.

## Format Descriptions (Data Models)

- For formats and descriptions corresponding Data Models have to be defined. (One could consider using pydantic for Data Models to validate data and also emit JSON/YAML)
- One should not redefine naming that are already defined in the official software or documentation (e.g. regmaps from FELIX or field names from lpGBT).
- It is important to have single source of truth.

### Naming for FE Fields and Registers 
naming/schema for bit fields and registers for chips.

Based on these descriptions corresponding C++ or python binding code will be auto-generated.
Backward compatibility will be supported.

- ITkPix v1/v2 chips, HCC/ABC Star chips, AMAC chip, lpGBT, GBCR, VTRX

### Raw/Processed/Message/Data Formats/Schema
formats for the data-taking, scan raw data and analysis results output, describe messages, histograms, hits, that can be passed between processes:

- Data-taking formats (Pixel/Strips)
- Raw histogram format for scan output (Pixel/Strips)
- Analysis results format (Pixel/Strips)

### Description of Scans
list of all scans with configuration and loops.

- Pixel Scan description, Strips Scan description

## ITk Packages

Proposed packages are software deliverables in terms of work organization. The work packages definition
will follow a similar structure, but not necessarily with a one-to-one correspondence.

### RegMap/FieldMap package 
Register Map, Register and Field common interfaces and functionality, based on Description of Fields and Registers:

- ITkPix v1/v2 chip, HCC/ABC Star chips, AMAC chip, FELIX registers, lpGBT, GBCR, VTRX

### Front-End Chip packages
provides common classes and routines for chip configuration and command generation, including data format and command format description:

- ITkPix v1/v2 chips, HCC/ABC Star chips, AMAC chip
- Several packages: one for pixel, one for strips

### FE Encoder/Decoder package
provides core encoding/decoding functionality:

- ITkPix v1/v2 chips decoder, HCC/ABC Star chips decoder

### Scan package
provides chip specific scan engines, raw hits accumulator and various loops implementation, scan raw data formats. Requires front-end description, description of scans, data-format description:

- Pixel scans (ITkPix v1/v2), Strips scans (Star chips)

### Controller
provides common controller interface based on FelixClient interface:

- FELIX Controller, FileDump

### FELIX Emulator
provides FELIX network interface for chip emulators to allow to use chip emulators like connected to felix-toflx, felix-tohost. Requires a FelixClient interface.

### FE Chip Emulator package
provides emulator for chips functionality, front-end description, command and data decoding.

- ITkPix v1/v2 chip 
- HCC/ABC Star chips
- AMAC Star chip

### SWROD plugin – provides SWROD plugin. Require front-end description, decoder, and SWROD

- Pixel SWROD plugin
- Strips SWROD plugin

### Trickle Configuration package – provides common trickle configuration routines:

- Trickle Pixel
- Trickle Strips

### FE Config/Connectivity/Results DB
package to access FE configuration, connectivity information and scan raw data and analysis results DB, based on front-end description, ...

Pixel config DB, Strips config DB

### Plotting tools – provides tools and routines to make plots based on raw scan data and analysis results. Requires front-end description, and data format description:
Pixel Plotting Tools, Strips Plotting Tools

### lpGBT communication package
communication over IC/EC, I2C ic_com, lpgbt_com, …

### Optoboard package
optoboard configuration tools, based on lpGBT communication package.

### TTC Controllable
configures the front-ends and inside a partition. Links to configuration, optoboard, IS, stopless recovery, DCS, TTC, ALTI, trickle configuration, …

- Pixel controllable (TTC)
- Strips controllable (TTC)

### DCS
DCS (OPC Servers)

- Pixel DCS
- Strips DCS

### Algorithms/Analysis package
provides common data processing algorithms and scan data analysis tools. Requires scan description, data format description, front-end description:

- Pixel analysis
- Strips analysis

### Online monitoring algorithms – provides online monitoring routines.
Requires front-end description, data format, OH, IS, … :

- Pixel online monitoring, Strips online monitoring

### Offline Calibration Analysis package
provides offline calibration analysis.

Based on Algorithms/Analysis package:

- Pixel offline calibration analyses
- Strips offline calibration analyses


```mermaid

graph LR;
    
    F{FE Commander
Config Manager
Front-End Chip
Trickle Config}

F-->LTI

DCS{FE Chip DCS}-->F

S{Scan Control
Data Acquisition
TTC}-->F
S-->DCS
S-->L
S-->CONFDB

GUI{GUI/Calib control
DB Manipulation
}-->S


L{lpGBT Config
lpGBT IC/EC, I2C
Optoboard, Stave/AMAC}-->DCS
L-->CONFDB

F{RX/TX, IC/EC
FELIX
FelixClient Intf
}

D{
Data Handler
FE Decoder
SWROD plugin
Raw Histogrammer
Data Acquisition
}-->H

H{Histogrammer
Algorithms/Analysis
}-->C

H-->OA{Online Analysis
Online Monitoring
Monitoring Recovery
}
H-->FDB
OA-->S

C{Calibration results DB}-->A

A{Calibration Analysis
Algorithms}-->C{Condition DB}
A-->FDB

FDB{FE Calib DB}

CONFDB{FE Config DB
FE Connectivity
}

```
